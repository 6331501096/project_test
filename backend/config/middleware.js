const {check, validationRes} = require('express-validator')
module.exports = function(req, res ,next){
    req.validate = function(){
        const errors = validationRes(req).array()
        if(errors.length == 0)return 
        throw new Error(`${errors[0].param} : ${errors[0].msg}`)
    }
    res.error = function(exx, status = 400){
        res.status(status).json({
            massage:exx.massage
        })
    }
    next()
}

