const express = require("express")
const server = express()
const PORT = 8000
const router = require('./routes')
const bodyParser = require('body-parser')
const expressSession = require('express-session')
const cors = require('cors')
const productRouter = require( './routes/product.js')

server.use(cors())
server.use(expressSession({
    secret: 'protal-admin',
    resave: false,
    saveUninitialized: true,
    cookie: {},
}))
server.use(bodyParser.urlencoded({
    extended: false,
    limit: '100MB'
}))
server.use(bodyParser.json({
    limit: '100MB'
}))

server.use('/api_protal', router)
server.use('/api/product',productRouter)
server.use(require('./config/middleware'))


server.listen(PORT, () => {
    console.log(`Sever is ready on port ${PORT}`);
});





