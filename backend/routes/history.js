const express = require('express')
const router = express.Router()
const historyService = require('../services/history.js')
router.get('/gethistory',historyService.getHistory)
router.get('/getHistoryEdit',historyService.getHistoryEdit)
router.get('/getHistoryEx',historyService.getHistoryEx)
router.get('/searchhistory/:id',historyService.searchHistory)
module.exports = router 