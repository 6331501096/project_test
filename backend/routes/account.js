const router = require('express').Router()
const con = require('../config/database')
const accountService = require('../services/account.js')
router.get('/checkservice', async (res, req) => {
    try {
        res.send(201).json({ massage: "Welcome to account" })
    } catch (err) {
        res.err(err)
    }
})
router.post('/register', async (req, res) => {
    const { id, name, password, phonenum, email, address, role, actID } = req.body;
    const sql = "INSERT INTO `users`(USER_ID, USER_NAME, USER_PASSWORD, USER_PHONE_NUM, USER_EMAIL, USER_ADDRESS, USER_ROLE, ACT_ID) VALUES (?,?,?,?,?,?,?,?)"
    try {
        con.query(sql,
            [id, name, password, phonenum, email, address, role, actID],
            (err, results, flelds) => {
                if (err) {
                    console.log('Error to Insert = ', err);
                    return res.status(400).send();
                }
                return res.status(201).json({ massage: "Insert data Successfull" })
            }
        )

    } catch (err) {
        console.log(err);
        return res.status(500).send();
    }
})
router.get('/getuser', async (req, res) => {
    const sql = "SELECT * FROM users"
    try {
        con.query(sql,(err, results, flelds) => {
                if (err) {
                    console.log(err);
                    return res.status(400).send(); 
                }
                return res.status(200).json(results)
            }
        )
    } catch (err) {
        console.log(err);
        return res.status(500).send();
    }
})
router.get('/getusername/:id', async (req, res) => {
    const id = req.params.id
    const sql = "SELECT * FROM users WHERE USER_ID = ?"
    try {
        con.query(sql,[id],(err, results, flelds) => {
                if (err) {
                    console.log(err);
                    return res.status(400).send(); 
                }
                return res.status(200).json(results)
            }
        )
    } catch (err) {
        console.log(err);
        return res.status(500).send();
    }
})   
router.get('/getusername/:userid/:password', async (req, res) => {
    const userid = req.params.userid
    const password = req.params.password
    const sql = "SELECT * FROM users WHERE USER_ID = ? AND USER_PASSWORD = ?"
    try {
        con.query(sql,[userid, password],(err, results, flelds) => {
                if (err) {
                    console.log(err);
                    return res.status(400).send(); 
                }
                return res.status(200).json(results)
            }
        )
    } catch (err) {
        console.log(err);
        return res.status(500).send();
    }
})
router.get('/searchuser/:id',accountService.searchuser)
router.put('/updateuser',accountService.putUpdateuser)
router.put('/activestatus/:id',accountService.putActiveuser)
router.put('/deactivestatus/:id',accountService.putDeactiveuser)

module.exports = router;