const express = require('express')
const router = express.Router()
const sellService = require('../services/sell.js')
router.get('/getIdLot/:id/:lot',sellService.getIdLot)
router.get('/getAll',sellService.getAll)
router.get('/getAllLot',sellService.getAllLot)
router.get('/getAllId',sellService.getAllId)
router.post('/addInvoice',sellService.addInvoice)
module.exports = router