const express = require('express')
const router = express.Router()
const homeservice = require('../services/home.js')
router.get('/getAll',homeservice.getAll)
router.get('/getRemain/:id/',homeservice.getRemain)
module.exports = router 