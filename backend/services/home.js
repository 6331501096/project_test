const con = require('../config/database')
const getAll = (req,res) => {
    con.query(`SELECT p.PRO_ID, p.PRO_NAME, p.PRO_TYPE, p.PRO_SALE_PRICE, sh.PRO_REMAIN, pu.UNIT_NAME FROM product p JOIN stock_history sh ON p.PRO_ID = sh.PRO_ID JOIN supplier s ON s.SUP_ID = sh.SUP_ID JOIN product_unit pu ON pu.UNIT_ID = sh.UNIT_ID WHERE sh.ACT_ID = 1 GROUP BY p.PRO_NAME ORDER BY sh.PRO_ID ASC`, (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results)  
        } 

    })
}
const getRemain = (req,res) => {
    const id = req.params.id
    let sum = 0
    con.query(`SELECT SUM(PRO_AMOUNT) TOTAL FROM stock_history WHERE ACT_ID = 1 AND MOV_ID = 1 AND PRO_ID = ? `,[id], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            // res.json(results)
            if(results[0].TOTAL == null){
                sum += 0
            }
            else{
                sum += results[0].TOTAL
            }
        } 
    })  
    con.query(`SELECT SUM(PRO_AMOUNT) ADD_TOTAL FROM stock_history WHERE MOV_ID = 3 AND PRO_ID = ? `,[id], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            // res.json(results)
            if(results[0].ADD_TOTAL == null){
                sum += 0
            }
            else{
                sum += results[0].ADD_TOTAL
            } 
        } 
    })  
    con.query(`SELECT SUM(PRO_AMOUNT) DECREASE_TOTAL FROM stock_history WHERE MOV_ID = 4 AND PRO_ID = ?`,[id], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            // res.json(results)
            if(results[0].DECREASE_TOTAL == null){
                sum += 0
            }
            sum -= results[0].DECREASE_TOTAL
        } 
    })  
    con.query(`SELECT SUM(PRO_AMOUNT) SELL_TOTAL FROM stock_history WHERE MOV_ID = 2 AND PRO_ID = ?`,[id], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            if(results[0].SELL_TOTAL == null){
                sum += 0
            }
            else{
                sum -= results[0].SELL_TOTAL
            }
            res.json(sum)
        } 
    })  
} 
module.exports = {getAll,getRemain} 