const con = require('../config/database')
const getIdLot = (req, res) => {
    const id = req.params.id
    const lot = req.params.lot
    let array = ''
    con.query(`SELECT * FROM stock_history sh JOIN product p ON p.PRO_ID = sh.PRO_ID JOIN product_unit pu ON pu.UNIT_ID = sh.UNIT_ID WHERE sh.ACT_ID = 1 AND sh.PRO_ID = ? AND sh.PRO_LOT = ? AND sh.MOV_ID = 1`,[id,lot], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            array = results[0]
            res.json(array)
        } 
    })  
} 
const addInvoice = (req, res) => {
    const {saleprice,amount,id,userid,supid,unit,lot,invo} = req.body
    const date = `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`;
    const time = `${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`
    con.query(
        `INSERT INTO stock_history (PRO_DATE, PRO_TIME, PRO_BUY_PRICE, PRO_AMOUNT, PRO_ID, USER_ID, SUP_ID, MOV_ID, ACT_ID, UNIT_ID, PRO_LOT, PRO_INV) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)`,
        [date, time, saleprice, amount, id, userid, supid, 2, 1, unit, lot, invo],
        (err, results) => {
            if (err) {
                console.error(err);
            } else {
                res.json(results);
            }  
        }
    );
}
const getAll = (req, res) => {
    con.query(`SELECT sh.PRO_INV, DATE_FORMAT(sh.PRO_DATE, '%Y/%m/%d') AS PRO_DATE, sh.PRO_TIME, SUM(sh.PRO_BUY_PRICE)*sh.PRO_AMOUNT AS TOTAL_PRICE,SUM(sh.PRO_AMOUNT) AS PRO_AMOUNT, u.USER_NAME FROM stock_history sh JOIN product p ON p.PRO_ID = sh.PRO_ID JOIN users u ON u.USER_ID = sh.USER_ID WHERE sh.MOV_ID = 2 GROUP BY sh.PRO_INV`, (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results)
        } 
    })  
} 
const getAllLot = (req, res) => {
    con.query(`SELECT PRO_LOT FROM stock_history WHERE ACT_ID = 1 AND MOV_ID = 1 GROUP BY PRO_LOT `, (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results)
        } 
    })  
} 
const getAllId = (req, res) => {
    con.query(`SELECT PRO_ID FROM stock_history WHERE ACT_ID = 1 AND MOV_ID = 1 GROUP BY PRO_ID`, (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results)
        } 
    })  
} 
module.exports = {getIdLot,addInvoice,getAll,getAllLot,getAllId} 