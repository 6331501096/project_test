const con = require('../config/database')
const getHistory =  (req, res) => {
    const sql ="SELECT sh.PRO_LOT, sh.PRO_ID, p.PRO_NAME, p.PRO_TYPE, s.SUP_NAME, DATE_FORMAT(sh.PRO_DATE, '%Y/%m/%d') AS PRO_DATE, sh.PRO_TIME,sh.PRO_AMOUNT,pu.UNIT_NAME FROM stock_history sh JOIN product p ON sh.PRO_ID = p.PRO_ID JOIN supplier s ON sh.SUP_ID = s.SUP_ID JOIN product_unit pu ON sh.UNIT_ID = pu.UNIT_ID WHERE sh.MOV_ID = 1 ORDER BY sh.PRO_DATE DESC, sh.PRO_TIME DESC"
    con.query(sql, (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
            
        }
    });
}
const getHistoryEdit =  (req, res) => {
    const sql ="SELECT sh.PRO_LOT, sh.PRO_ID, p.PRO_NAME, p.PRO_TYPE, DATE_FORMAT(sh.PRO_DATE, '%Y/%m/%d') AS PRO_DATE, sh.PRO_TIME,sh.PRO_AMOUNT, u.USER_NAME, sh.PRO_ISSUE, sh.MOV_ID FROM stock_history sh JOIN product p ON sh.PRO_ID = p.PRO_ID JOIN users u ON sh.USER_ID = u.USER_ID WHERE sh.MOV_ID = 3 OR sh.MOV_ID = 4 ORDER BY sh.PRO_DATE DESC, sh.PRO_TIME DESC"
    con.query(sql, (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
            
        }
    });
}
const getHistoryEx =  (req, res) => {
    const sql ="SELECT sh.PRO_LOT, sh.PRO_ID, p.PRO_NAME, p.PRO_TYPE, DATE_FORMAT(sh.PRO_DATE, '%Y/%m/%d') AS PRO_DATE, sh.PRO_TIME,sh.PRO_AMOUNT, sh.PRO_BUY_PRICE, pu.UNIT_NAME FROM stock_history sh JOIN product p ON sh.PRO_ID = p.PRO_ID JOIN users u ON sh.USER_ID = u.USER_ID JOIN product_unit pu ON pu.UNIT_ID = sh.UNIT_ID WHERE sh.MOV_ID = 2 ORDER BY sh.PRO_DATE DESC, sh.PRO_TIME DESC"
    con.query(sql, (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
            
        }
    });
}
const searchHistory =  (req, res) => {
    const id = "%"+req.params.id+"%"
    const sql ="SELECT sh.PRO_LOT, sh.PRO_ID, p.PRO_NAME, p.PRO_TYPE, s.SUP_NAME, DATE_FORMAT(sh.PRO_DATE, '%Y/%m/%d') AS PRO_DATE, sh.PRO_TIME,sh.PRO_AMOUNT,pu.UNIT_NAME FROM stock_history sh JOIN product p ON sh.PRO_ID = p.PRO_ID JOIN supplier s ON sh.SUP_ID = s.SUP_ID JOIN product_unit pu ON sh.UNIT_ID = pu.UNIT_ID WHERE sh.PRO_ID LIKE ? AND sh.MOV_ID = 1 ORDER BY sh.PRO_DATE DESC, sh.PRO_TIME DESC "
    con.query(sql,id, (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
            
        }
    });
}
module.exports = {getHistory,searchHistory,getHistoryEdit,getHistoryEx}

       