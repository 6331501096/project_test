const con = require('../config/database')
//  get from stock history
const getproduct = (req, res) => {
    const id = req.params.id
    con.query('SELECT p.PRO_NAME, p.PRO_TYPE, p.UNIT_ID, pu.UNIT_NAME FROM product p JOIN product_unit pu ON p.UNIT_ID = pu.UNIT_ID WHERE PRO_ID = ?', [id], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results)
        }
    })
}
const searchproductid = (req, res) => {
    const id = req.params.id
    con.query(`SELECT PRO_ID FROM product WHERE PRO_ID LIKE '%${id}%'`, (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results)
        }
    })
}
const getproductidAll = (req, res) => {
    con.query('SELECT PRO_ID FROM product', (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results)
        }
    })
}

const getproductID = (req, res) => {
    const id = req.params.id
    const sql = "SELECT PRO_ID FROM product WHERE PRO_ID = ?"
    try {
        con.query(sql, [id], (err, results, flelds) => {
            if (err) {
                console.log(err);
                return res.status(400).send();
            }
            return res.status(200).json(results)
        }
        )
    } catch (err) {
        console.log(err);
        return res.status(500).send();
    }
}
const createproduct = (req, res) => {
    const newProduct = req.body;
    con.query("INSERT INTO product SET ?", newProduct, (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}

// const showinventory = (req, res) => {
//     const sql = "SELECT sh.PRO_Index, sh.PRO_ID,sh.PRO_LOT, p.PRO_NAME, p.PRO_TYPE, DATE_FORMAT(sh.PRO_EXP, '%Y-%m-%d'), sh.PRO_BUY_PRICE, p.PRO_SALE_PRICE, sh.PRO_REMAIN, pu.UNIT_NAME FROM stock_history sh JOIN product p ON sh.PRO_ID = p.PRO_ID JOIN product_unit pu ON sh.UNIT_ID = pu.UNIT_ID WHERE sh.ACT_ID  = 1"
//     con.query(sql, (err, results) => {
//         if (err) {
//             console.error(err);
//         }
//         else {
//             res.json(results);
//         }
//     });
// }
const showinventory = (req, res) => {
    const sql = "SELECT sh.PRO_Index, sh.PRO_ID, sh.PRO_LOT, p.PRO_NAME, p.PRO_TYPE, DATE_FORMAT(sh.PRO_EXP, '%Y/%m/%d') AS PRO_EXP, sh.PRO_BUY_PRICE, p.PRO_SALE_PRICE, sh.PRO_REMAIN, pu.UNIT_NAME FROM stock_history sh JOIN product p ON sh.PRO_ID = p.PRO_ID JOIN product_unit pu ON sh.UNIT_ID = pu.UNIT_ID WHERE sh.ACT_ID = 1 AND sh.MOV_ID = 1 ORDER BY sh.PRO_ID ASC" ;
    con.query(sql, (err, results) => {
        if (err) {
            console.error(err);
        } else {
            res.json(results);
        }
    });
}
const searchProduct = (req, res) => {
    const id = "%"+req.params.id+"%"
    const sql = "SELECT sh.PRO_Index, sh.PRO_ID,sh.PRO_LOT, p.PRO_NAME, p.PRO_TYPE, DATE_FORMAT(sh.PRO_EXP, '%Y/%m/%d') AS PRO_EXP, sh.PRO_BUY_PRICE, p.PRO_SALE_PRICE, sh.PRO_REMAIN, pu.UNIT_NAME FROM stock_history sh JOIN product p ON sh.PRO_ID = p.PRO_ID JOIN product_unit pu ON sh.UNIT_ID = pu.UNIT_ID WHERE sh.ACT_ID  = 1 AND (sh.PRO_ID LIKE ? ) AND MOV_ID = 1"
    con.query(sql,[id,], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
const getunit = (req, res) => {
    con.query("SELECT * FROM product_unit", (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
const addproduct = (req, res) => {
    const PRO_DATE = `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`;
    const PRO_LOT = req.body.PRO_LOT; 
    const PRO_EXP = req.body.PRO_EXP; 
    const PRO_TIME = `${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`
    const PRO_BUY_PRICE = req.body.PRO_BUY_PRICE; 
    const PRO_REMAIN = req.body.PRO_REMAIN;
    const PRO_AMOUNT = req.body.PRO_AMOUNT; 
    const PRO_ID = req.body.PRO_ID; 
    const USER_ID = req.body.USER_ID; 
    const SUP_ID = req.body.SUP_ID; 
    const UNIT_ID = req.body.UNIT_ID; 
    con.query(
      'INSERT INTO stock_history (PRO_DATE, PRO_LOT, PRO_EXP, PRO_TIME, PRO_BUY_PRICE, PRO_REMAIN, PRO_AMOUNT, PRO_ID, USER_ID, SUP_ID, MOV_ID, ACT_ID, UNIT_ID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, 1, ?)',
      [
        PRO_DATE,
        PRO_LOT,
        PRO_EXP,
        PRO_TIME,
        PRO_BUY_PRICE,
        PRO_REMAIN,
        PRO_AMOUNT,
        PRO_ID,
        USER_ID,
        SUP_ID,
        UNIT_ID,
      ],
      (err, results) => {
        if (err) {
          console.error(err);
          res.status(500).send('Error adding product to the database');
        } else {
          res.json(results);
        }
      }
    );
  };
  const deactiveProduct = (req, res) => {
    const index = req.params.index
    con.query("UPDATE `stock_history` SET `ACT_ID`= 0 WHERE PRO_Index = ?",[index], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
  const changeamount= (req, res) => {
    const {lot,amount,proid,userid,moveid,unitid,supid,issue} = req.body
    const date = `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`;
    const time = `${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`
    con.query("INSERT INTO `stock_history` (PRO_LOT, PRO_DATE, PRO_TIME, PRO_AMOUNT, PRO_ID, USER_ID, MOV_ID, ACT_ID, UNIT_ID, SUP_ID, PRO_ISSUE) VALUES (?, ?, ?, ?, ?, ?, ?, 1, ?, ?, ?)",[lot,date,time,amount,proid,userid,moveid,unitid,supid,issue], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
  const selectSupidFromlot= (req, res) => {
    const id = req.params.id
    const lot = req.params.lot
    con.query("SELECT SUP_ID FROM `stock_history` WHERE PRO_ID = ? AND PRO_LOT = ?",[id,lot], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
  const selectremian= (req, res) => {
    const id = req.params.id
    const lot = req.params.lot
    let sum = 0
    con.query("SELECT PRO_REMAIN AS TOTAL FROM `stock_history` WHERE ACT_ID = 1 AND MOV_ID = 1 AND PRO_ID = ? AND PRO_LOT = ?",[id,lot], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            // res.json(results)
            if(results[0] == null){
                sum += 0
            }
            else{
                sum += results[0].TOTAL
            }
        }
    });
    con.query("SELECT SUM(PRO_AMOUNT) ADD_TOTAL FROM `stock_history` WHERE MOV_ID = 3 AND PRO_ID = ? AND PRO_LOT = ?",[id,lot], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            if(results[0].ADD_TOTAL == null){
                sum += 0
            }
            else{
                sum += results[0].ADD_TOTAL
            }
        }
    });
    con.query("SELECT SUM(PRO_AMOUNT) SELL_TOTAL FROM `stock_history` WHERE MOV_ID = 2 AND PRO_ID = ? AND PRO_LOT = ?",[id,lot], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            if(results[0].SELL_TOTAL == null){
                sum += 0
            }
            else{
                sum -= results[0].SELL_TOTAL
            }
        }
    });
    con.query("SELECT SUM(PRO_AMOUNT) DECREASE_TOTAL FROM `stock_history` WHERE MOV_ID = 4 AND PRO_ID = ? AND PRO_LOT = ?",[id,lot], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            if(results[0].DECREASE_TOTAL == null){
                sum += 0
            }
            else{
                sum -= results[0].DECREASE_TOTAL
            }
            res.json(sum);
        }
    });
}
const showMasterPro = (req, res) => {
    con.query("SELECT * FROM product p JOIN product_unit pu ON pu.UNIT_ID = p.UNIT_ID ", (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        } 
    });
}
const getUnitidfromName = (req, res) => {
    const name = req.params.name
    con.query("SELECT UNIT_ID FROM product_unit WHERE UNIT_NAME = ? ",[name], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
module.exports = { getproduct, getproductID, createproduct, showinventory, getunit, searchproductid, getproductidAll, addproduct, deactiveProduct,searchProduct,changeamount,selectremian,selectSupidFromlot,showMasterPro,getUnitidfromName}