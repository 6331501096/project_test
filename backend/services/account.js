const con = require('../config/database')
const putUpdateuser =  (req, res) => {
    const {name,password,phonenum,email,address,role,id} = req.body;
    con.query("UPDATE `users` SET `USER_NAME`= ?,`USER_PASSWORD`= ?,`USER_PHONE_NUM`= ?,`USER_EMAIL`= ?,`USER_ADDRESS`= ?,`USER_ROLE`= ? WHERE USER_ID = ?", [name,password,phonenum,email,address,role,id], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
const putActiveuser =  (req, res) => {
    const id = req.params.id;
    con.query("UPDATE `users` SET `ACT_ID`= 1 WHERE USER_ID = ?", [id], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
const searchuser = (req, res) => {
    const id = req.params.id
    con.query(`SELECT * FROM users WHERE USER_ID LIKE '%${id}%'`, (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results)
        }
    })
}
const putDeactiveuser =  (req, res) => {
    const id = req.params.id;
    con.query("UPDATE `users` SET `ACT_ID`= 0 WHERE USER_ID = ?", [id], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
module.exports = {putActiveuser,putDeactiveuser,putUpdateuser,searchuser}

       