const con = require('../config/database')
const getsupplier = (req, res) => {
    con.query("SELECT * FROM supplier", (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
const searchsupplier = (req, res) => {
    const id = req.params.id
    con.query(`SELECT * FROM supplier WHERE SUP_ID LIKE '%${id}%'`, (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results)
        }
    })
}
const getsupplierid = (req, res) => {
    con.query("SELECT SUP_ID ,ACT_ID FROM supplier", (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
const getsupplierinf = (req, res) => {
    const id = req.params.id
    con.query("SELECT * FROM supplier WHERE SUP_ID = ?",[id], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
const putUpdate =  (req, res) => {
    const {id,name,email,address,tell} = req.body;
    con.query("UPDATE `supplier` SET `SUP_NAME`= ?,`SUP_EMAIL`= ?,`SUP_ADDRESS`= ?, `SUP_TELL`= ?  WHERE `SUP_ID` = ?", [name,email,address,tell,id], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
const putActiveSupplier =  (req, res) => {
    const id = req.params.id;
    con.query("UPDATE `supplier` SET `ACT_ID`= 1 WHERE SUP_ID = ?", [id], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
const putDeactiveSupplier =  (req, res) => {
    const id = req.params.id;
    con.query("UPDATE `supplier` SET `ACT_ID`= 0 WHERE SUP_ID = ?", [id], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
const CreateSupplier =  (req, res) => {
    const {id,name,email,address,tell} = req.body;
    con.query("INSERT INTO `supplier`(`SUP_ID`, `SUP_NAME`, `SUP_ADDRESS`, `SUP_TELL`, `SUP_EMAIL`, `ACT_ID`) VALUES (?,?,?,?,?,1)", [id,name,email,address,tell], (err, results) => {
        if (err) {
            console.error(err);
        }
        else {
            res.json(results);
        }
    });
}
module.exports = {getsupplier,getsupplierid,getsupplierinf,putActiveSupplier,putDeactiveSupplier,CreateSupplier,putUpdate,searchsupplier}