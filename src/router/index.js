import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Dashboard from '../views/Dashboard.vue'
import Inventory from '../views/Inventory.vue'
import History from '../views/History.vue'
import Historyexport from '../views/Historyexport.vue'
import HistoryEdit from '../views/HistoryEdit.vue'
import Sell from '../views/Sell.vue'
import User from '../views/UserVue.vue'
import Supplier from '../views/Supplier.vue'
import Productcome from '../views/Productcome.vue'
import CreateUser from '../views/CreateUser.vue'
import TestVue from '../views/Test.vue'
import HomePage from '../views/HomePage.vue'
import SellAdd from '../views/SellAdd.vue'
import Invoice from '../views/Invoice.vue'
import SellMore from '../views/SellMore.vue'
import AddProduct from '../views/Addproduct.vue'
// import AddProduct from '../views/AddProduct.vue'

Vue.use(VueRouter)
const routes = [
    {
        path: '/',
        name: 'LoginVue',
        component: Login
    }, {
        path: '/dashboard',
        name: 'DashboardVue',
        component: Dashboard
    }, {
        path: '/inventory',
        name: 'InventoryVue',
        component: Inventory
    }, {
        path: '/history',
        name: 'HistoryVue',
        component: History
    }, {
        path: '/historyexport',
        name: 'HistoryExportVue',
        component: Historyexport
    }, {
        path: '/sell',
        name: 'SellVue',
        component: Sell
    }, {
        path: '/user',
        name: 'UserVue',
        component: User
    }, {
        path: '/create_user',
        name: 'CreateUser',
        component: CreateUser
    }, {
        path: '/supplier',
        name: 'SupplierVue',
        component: Supplier
    }, {
        path: '/productcome',
        name: 'ProductcomeVue',
        component: Productcome
    },{
        path: '/test',
        name: 'TestVue',
        component: TestVue
    },{
        path: '/homepage',
        name: 'HomePage',
        component: HomePage
    },{
        path: '/selladd',
        name: 'SellAdd',
        component: SellAdd
    },{
        path: '/invoice',
        name: 'Invoice',
        component: Invoice
    },{
        path: '/historyedit',
        name: 'HistoryEdit',
        component: HistoryEdit
    },{
        path: '/sellmore',
        name: 'SellMore',
        component: SellMore
    },{
        path: '/addproduct',
        name: 'AddProduct',
        component: AddProduct
    }
]
const router = new VueRouter({
    routes
})
export default router